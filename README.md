# Raspberry Pi Playground Workspace

this workspace will ultimately hold a number of NodeJS-based projects, adding libraries and applications as I work through various ideas.

one of the biggest points to focus on here is GPIO interactions.. more specifically, trying out different ways that physical inputs and outputs can be linked to "program business logic".

## Planned Projects

so in no particular order, and subject to change based on my fleeting whims and desires, I currently plan to make use of the following projects:

* [ ] [Cylon.js](https://github.com/hybridgroup/cylon/)
  * also found a pretty interesting reference project.. a simple CRA frontend, with an Express backend that implements Cylon.js: [avoram/explore-react-cylon-integration](https://github.com/avoram/explore-react-cylon-integration)
* [ ] [onoff](https://github.com/fivdi/onoff)
  * this appears to be the most up-to-date and currently maintained Node GPIO library available at the time of writing this. it's also been kept as generic as possible, requiring additional library modules for platform-specific functionality.
* [ ] [my old `rpi-playground`](https://gitlab.com/zalithka/old-rpi-playground)
  * which contains a simple-ish NodeJS script where I was playing with controlling LEDs through code, triggered by momentary push-buttons.

## Bonus Ideas

I discovered a [very interesting post](https://www.raspberrypi.org/blog/gpio-expander/) on the RPi blog, which describes the process of interacting with the GPIO header on a Raspberry Pi Zero from a PC via USB..

this does **specifically** require a Pi Zero, as noted in the article. however, the author does go on to state:

> You can also install the application on the Raspberry Pi. Theoretically, you could connect a number of Pi Zeros to a single Pi and (without a USB hub) use a maximum of 140 pins! But I’ve not tested this — one for you, I think…

controlling *140* GPIO pins might be considered overkill for a playground, but the concept of expanding the GPIO pins of a standard RPi with those of a "slaved" RPi Zero really does sound fascinating.

furthermore, running "Raspberry Pi Desktop" in a VM on my dev machine, whilst giving the OS seamless access to the Zero's GPIO header, sounds like a much more pleasant way to dev for the Pi.

> **note**: while the OP topic is specific to MacOS, [this forum reply](https://www.raspberrypi.org/forums/viewtopic.php?t=226697#p1390920) contains some very helpful information regarding USB pass-through with VirtualBox.
